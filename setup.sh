#/bin/env bash

set -xe

# Install a bunch of tools
apt-get update && apt-get install p7zip-full wget tar -y

# Get rockyou.txt
wget knt.li/rockyou
tar -xzf rockyou
rm rockyou

# Get the 1000 most used words in the english language
wget https://gist.githubusercontent.com/deekayen/4148741/raw/98d35708fa344717d8eee15d11987de6c8e26d7d/1-1000.txt

# Get the Battlefield hashes
wget http://www.adeptus-mechanicus.com/codex/hashpass/bfield.hash.7z
7z x bfield.hash.7z
rm bfield.hash.7z

# Get hashcat's example files
wget https://github.com/hashcat/hashcat/raw/master/example0.hash
wget https://github.com/hashcat/hashcat/raw/master/rules/dive.rule

