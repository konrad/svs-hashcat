# Hashcat Examples

A bunch of hashcat examples used in the [SvS presentation on password security](https://kolaente.dev/uni/svs-passwords).

All examples were tested to run in [this docker image](https://github.com/dizcza/docker-hashcat).

